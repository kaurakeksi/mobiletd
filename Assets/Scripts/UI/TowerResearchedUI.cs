﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TowerResearchedUI : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    [SerializeField]
    private float _visibleTime;

    private void OnEnable()
    {
        TowerBuilder.OnTowerResearched += OnTowerResearched;
        _text.enabled = false;
    }

    private void OnDisable()
    {
        TowerBuilder.OnTowerResearched -= OnTowerResearched;
    }

    public void OnTowerResearched(Tower tower, int amount)
    {
        _text.text = tower.Name + " researched!";
        _text.enabled = true;

        StopAllCoroutines();
        StartCoroutine(HideAfterSeconds(_visibleTime));
    }
    
    public void OnWaveEnded(int wave)
    {
        _text.text = "Wave " + wave.ToString()  + " cleared!";
        _text.enabled = true;
    }

    private IEnumerator HideAfterSeconds(float delay)
    {
        float startTime = Time.time;
        while (Time.time < startTime + delay)
        {
            yield return null;
        }

        _text.enabled = false;
    }
}
