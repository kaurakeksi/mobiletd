﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveClearedUI : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    [SerializeField]
    private float _visibleTime;

    private void OnEnable()
    {
        _text.enabled = false;
        Spawner.OnWaveEnded += OnWaveEnded;
    }

    private void OnDisable()
    {
        Spawner.OnWaveEnded -= OnWaveEnded;
    }
    
    public void OnWaveEnded(int wave)
    {
        if (GameManager.Instance.GameOver)
        {
            return;
        }
        
        _text.text = "Wave " + wave.ToString()  + " cleared!";
        _text.enabled = true;

        StopAllCoroutines();
        StartCoroutine(HideAfterSeconds(_visibleTime));
    }

    private IEnumerator HideAfterSeconds(float delay)
    {
        float startTime = Time.time;
        while (Time.time < startTime + delay)
        {
            yield return null;
        }

        _text.enabled = false;
    }
}
