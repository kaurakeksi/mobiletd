﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FloatingText : MonoBehaviour, ITimedUpdate
{
    [SerializeField]
    private float _floatingSpeed;

    [SerializeField]
    private float _lifeTime;

    private Text _text;
    private float _spawnTime;

    private Camera _camera;
    private Vector3 _worldPosition;

    private void Awake()
    {
        _text = GetComponent<Text>();
    }

    private void OnEnable()
    {
        TimeScaler.Instance.Subscribe(this);
    }

    private void OnDisable()
    {
        _spawnTime = 0.0f;
        _text.text = "";
        TimeScaler.Instance.Unsubscribe(this);
    }

    public void Initialize(string text, Color color, Vector3 position)
    {
        _text.text = text;
        _text.color = color;
        _camera = Camera.main;
        _worldPosition = position;
        transform.position = _camera.WorldToScreenPoint(_worldPosition);
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (_spawnTime <= 0.0f)
        {
            _spawnTime = totalTime;
        }

        transform.position = _camera.WorldToScreenPoint(_worldPosition + Vector3.up * (totalTime - _spawnTime) * _floatingSpeed);

        if (_spawnTime + _lifeTime < totalTime)
        {
            ObjectPoolManager.Instance.PoolOrDestroy(gameObject);
        }
    }
}
