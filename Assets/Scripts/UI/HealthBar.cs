using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(StatEntity))]
public class HealthBar : MonoBehaviour, ITimedUpdate
{
    [SerializeField]
    private GameObject _healthBarPrefab;
    private HealthBarUI _healthBarInstance;

    private Canvas _canvas;
    private StatEntity _entity;

    private void Awake()
    {
        _entity = GetComponent<StatEntity>();
        _canvas = FindObjectOfType<Canvas>();
    }

    public void OnEnable()
    {
        GameObject go = ObjectPoolManager.Instance.Instantiate(_healthBarPrefab, _canvas.transform);
        _healthBarInstance = go.GetComponent<HealthBarUI>();
        _healthBarInstance.transform.localScale = new Vector3(1, 1, 1);
        _healthBarInstance.Initialize(_entity);

        TimeScaler.Instance.Subscribe(this);
    }

    public void OnDisable()
    {
        TimeScaler.Instance.Unsubscribe(this);
        ObjectPoolManager.Instance.PoolOrDestroy(_healthBarInstance.gameObject);
        _healthBarInstance = null;
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (_healthBarInstance != null)
        {
            _healthBarInstance.TimedUpdate(totalTime, deltaTime);
        }
    }
}