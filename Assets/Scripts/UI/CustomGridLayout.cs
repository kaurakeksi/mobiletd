﻿using UnityEngine;
using UnityEngine.UI;

public enum FitType
{
    Uniform,
    Width,
    Height,
    FixedRows,
    FixedColumns
}

public class CustomGridLayout : LayoutGroup
{
    public FitType FitType;
    public int Rows;
    public int Columns;
    public Vector2 CellSize;
    public Vector2 Spacing;
    
    public bool FitX;
    public bool FitY;

    public bool UseParentSizeAsBase;

    public bool UseChildPreferredSizeX;
    public bool UseChildPreferredSizeY;

    public bool SetContentObjectSize;

    private RectTransform _parentRectTransform;

    protected override void Awake()
    {
        _parentRectTransform = transform.parent.GetComponent<RectTransform>();
    }

    public override void CalculateLayoutInputHorizontal()
    {
        base.CalculateLayoutInputHorizontal();

        if (UseParentSizeAsBase && _parentRectTransform.gameObject != transform.parent.gameObject)
        {
            _parentRectTransform = transform.parent.GetComponent<RectTransform>();
        }

        if (FitType == FitType.Width || FitType == FitType.Height || FitType == FitType.Uniform)
        {
            float sqrt = Mathf.Sqrt(transform.childCount);
            Rows = Mathf.CeilToInt(sqrt);
            Columns = Mathf.CeilToInt(sqrt);

            FitX = true;
            FitY = true;
        }

        if (FitType == FitType.Width || FitType == FitType.FixedColumns)
        {
            Rows = Mathf.CeilToInt(transform.childCount / (float)Columns);
        }
        else if (FitType == FitType.Height || FitType == FitType.FixedRows)
        {
            Columns = Mathf.CeilToInt(transform.childCount / (float)Rows);
        }


        float xSpacing = (Spacing.x / (float) Columns * (Columns - 1));
        float ySpacing = (Spacing.y / (float) Rows * (Rows - 1));
        float xPadding = (padding.right / (float)Columns) + (padding.right / (float)Columns);
        float yPadding = (padding.bottom / (float)Rows) + (padding.top / (float)Rows);

        float parentWidth = _parentRectTransform.rect.width;
        float parentHeight = _parentRectTransform.rect.height;
        float width = UseParentSizeAsBase ? parentWidth : rectTransform.rect.width;
        float height = UseParentSizeAsBase ? parentHeight : rectTransform.rect.height;
        float cellWidth = width / (float)Columns - xSpacing - xPadding;
        float cellHeight = height / (float)Rows - ySpacing - yPadding;
        CellSize.x = FitX ? cellWidth : CellSize.x;
        CellSize.y = FitY ? cellHeight : CellSize.y;

        float accumulatingX = 0.0f;
        float accumulatingY = 0.0f;
        for (int i = 0; i < rectChildren.Count; ++i)
        {
            int row = i / Columns;
            int column = i % Columns;

            RectTransform item = rectChildren[i];

            float sizeX = UseChildPreferredSizeX ? item.rect.width : CellSize.x;
            float sizeY = UseChildPreferredSizeY ? item.rect.height : CellSize.y;
            if (float.IsNaN(sizeX))
            {
                sizeX = 0.0f;
            }
            if (float.IsNaN(sizeY))
            {
                sizeY = 0.0f;
            }
            
            float sizeOffsetX = UseChildPreferredSizeX ? accumulatingX : CellSize.x * column;
            float sizeOffsetY = UseChildPreferredSizeY ? accumulatingY : CellSize.y * row;
            float x = sizeOffsetX + (Spacing.x * column) + padding.left;
            float y = sizeOffsetY + (Spacing.y * row) + padding.top;
            
            SetChildAlongAxis(item, 0, x, sizeX);
            SetChildAlongAxis(item, 1, y, sizeY);

            accumulatingX += sizeX;
            accumulatingY += sizeY;
        }

        if (SetContentObjectSize)
        {
            UseParentSizeAsBase = true;
            float sizeOffsetX = UseChildPreferredSizeX ? accumulatingX : CellSize.x * Columns;
            float sizeOffsetY = UseChildPreferredSizeY ? accumulatingY : CellSize.y * Rows;
            float sizeX = sizeOffsetX + xSpacing * Columns + padding.left + padding.right;
            float sizeY = sizeOffsetY + ySpacing * Rows + padding.top + padding.bottom;

            if (float.IsNaN(sizeX))
            {
                sizeX = 0.0f;
            }
            if (float.IsNaN(sizeY))
            {
                sizeY = 0.0f;
            }
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Max(_parentRectTransform.rect.width, sizeX));
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Mathf.Max(_parentRectTransform.rect.height, sizeY));
        }
    }

    public override void CalculateLayoutInputVertical()
    {
        
    }

    public override void SetLayoutHorizontal()
    {
        
    }

    public override void SetLayoutVertical()
    {

    }
}
