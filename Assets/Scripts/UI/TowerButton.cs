﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TowerButton : MonoBehaviour
{
    public static event Action<TowerButton, Tower> OnTowerButtonPressed = (button, tower) => { };

    [SerializeField]
    private Image _image;

    [SerializeField]
    private Text _amountText;
    
    [SerializeField]
    private Text _costText;

    [SerializeField]
    private Button _button;

    private int _cost;
    public int Cost
    {
        get
        {
            return _cost;
        }
        set
        {
            _cost = value;
            _costText.text = _cost.ToString();
        }
    }

    private int _amount;
    public int Amount
    {
        get
        {
            return _amount;
        }
        set
        {
            _amount = value;
            _amountText.text = _amount.ToString() + "x";
        }
    }

    private Tower _tower;
    public Tower Tower
    {
        get
        {
            return _tower;
        }
        set
        {
            _tower = value;
            if (_tower.Icon != null)
            {
                _image.sprite = _tower.Icon;
            }
            Cost = _tower.Cost;
        }
    }

    private void OnEnable()
    {
        _button.onClick.AddListener(OnClick);
    }

    private void OnDisable()
    {
        _button.onClick.RemoveListener(OnClick);
    }

    public void OnClick()
    {
        OnTowerButtonPressed(this, _tower);
    }
}