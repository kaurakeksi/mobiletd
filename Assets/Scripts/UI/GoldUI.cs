﻿using UnityEngine;
using UnityEngine.UI;

public class GoldUI : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    private void Update()
    {
        string gold = Mathf.FloorToInt(TowerBuilder.Instance.Gold).ToString();
        _text.text = "$ " + gold;
    }
}
