﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FloatingTextType
{
    Critical,
    Heal
}

public class FloatingTextManager : Singleton<FloatingTextManager>
{
    [SerializeField]
    private GameObject _textPrefab;

    private Canvas _canvas;

    private void Awake()
    {
        _canvas = FindObjectOfType<Canvas>();
    }

    private void OnEnable()
    {
        Tower.OnCrit += OnCrit;
    }

    private void OnDisable()
    {
        Tower.OnCrit -= OnCrit;
    }

    public void OnCrit(GameObject target, float damage)
    {
        CreateFloatingText(target.transform.position, damage.ToString() + "!", FloatingTextType.Critical);
    }

    public void CreateFloatingText(Vector3 position, string text, FloatingTextType textType)
    {
        var obj = ObjectPoolManager.Instance.Instantiate(_textPrefab, position, Quaternion.identity, _canvas.transform);
        if (obj != null)
        {
            Color color = FloatingTextColorLookup(textType);
            obj.GetComponent<FloatingText>().Initialize(text, color, position);
        }
    }

    private Color FloatingTextColorLookup(FloatingTextType textType)
    {
        if (textType == FloatingTextType.Critical)
        {
            return Color.red;
        }
        else if (textType == FloatingTextType.Heal)
        {
            return Color.green;
        }

        return Color.black;
    }
}