using System;
using UnityEngine;
using UnityEngine.UI;

public class TowerSkillEntry : MonoBehaviour
{
    [SerializeField]
    private Image _image;

    [SerializeField]
    private Text _nameText;
    
    [SerializeField]
    private Text _descriptionText;

    public void SetEntryText(string name, string description, Sprite icon)
    {
        _nameText.text = name;
        _descriptionText.text = description;

        if (icon != null)
        {
            _image.sprite = icon;   
        }
    }
}