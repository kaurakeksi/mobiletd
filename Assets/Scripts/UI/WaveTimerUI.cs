﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveTimerUI : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    private void Update()
    {
        if (Spawner.Instance.SpawningWave)
        {
            _text.text = "Wave " + Spawner.Instance.CurrentWave.ToString() + " spawning...";
        }
        else
        {
            string waveTimer = Mathf.CeilToInt(Mathf.Max(Spawner.Instance.TimeUntilNextWave - 1, 0)).ToString();
            _text.text = "Wave " + (Spawner.Instance.CurrentWave + 1).ToString() + " in " + waveTimer;
        }
    }
}
