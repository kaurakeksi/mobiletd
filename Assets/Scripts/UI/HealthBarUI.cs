﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    [SerializeField]
    private Slider _slider;

    [SerializeField]
    private Image _fill;

    [SerializeField]
    private Gradient _gradient;

    [SerializeField]
    private Vector3 _offset;

    private StatEntity _entity;
    private Camera _camera;
    
    public void Initialize(StatEntity entity)
    {
        _entity = entity;
        _camera = Camera.main;

        UpdateValues();
        UpdatePosition();
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (_entity != null)
        {
            UpdateValues();
            UpdatePosition();
        }
    }

    private void UpdateValues()
    {
        Stat hp = _entity.Stats.Get(StatNames.Hitpoints);
        if (hp != null)
        {
            _slider.maxValue = hp.MaxValue;
            _slider.value = hp.Value;
            
            _fill.color = _gradient.Evaluate(_slider.normalizedValue);
        }
    }

    private void UpdatePosition()
    {
        transform.position = _camera.WorldToScreenPoint(_entity.transform.position + _offset);
    }
}
