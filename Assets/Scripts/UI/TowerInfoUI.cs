﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerInfoUI : Singleton<TowerInfoUI>
{
    public static event System.Action<Tower> OnTowerUpgradeButtonPressed = (tower) => { };

    [Header("Tower info")]
    [SerializeField]
    private Text _towerNameText;

    [SerializeField]
    private Image _towerIcon;

    [SerializeField]
    private Text _towerLevelText;

    [SerializeField]
    private Text _towerExperienceText;

    [SerializeField]
    private Text _towerDamageText;

    [SerializeField]
    private Text _towerRangeText;

    [SerializeField]
    private Text _towerAttackSpeedText;

    [SerializeField]
    private Text _towerTriggerChanceText;

    [SerializeField]
    private Text _towerCritChanceText;

    [SerializeField]
    private Text _towerCritDamageText;

    [SerializeField]
    private Text _towerExpGainText;

    [SerializeField]
    private Text _towerGoldGainText;

    [Header("Upgrade")]
    [SerializeField]
    private Button _upgradeButton;

    [Header("Skill descriptions")]
    [SerializeField]
    private Scrollbar _skillScrollBar;

    [SerializeField]
    private GameObject _content;

    [SerializeField]
    private TowerSkillEntry _skillEntryPrefab;

    private Tower _tower;
    private List<TowerSkillEntry> _skillEntries = new List<TowerSkillEntry>();

    private void OnEnable()
    {
        _upgradeButton.onClick.AddListener(OnUpgradeButtonClicked);
    }

    private void OnDisable()
    {
        _upgradeButton.onClick.RemoveListener(OnUpgradeButtonClicked);
    }

    public void OnUpgradeButtonClicked()
    {
        OnTowerUpgradeButtonPressed(_tower);
    }

    private void Update()
    {
        if (_tower != null)
        {
            UpdateTowerInfo(false);
            if (TowerBuilder.Instance.CanUpgrade(_tower))
            {
                _upgradeButton.interactable = true;
            }
            else
            {
                _upgradeButton.interactable = false;
            }
        }
        else
        {
            _upgradeButton.interactable = false;
        }
    }

    public void SetInfo(Tower tower)
    {
        _tower = tower;
        UpdateTowerInfo(true);
    }

    private void UpdateTowerInfo(bool updateThroughly)
    {
        // Exp & level
        int level = _tower.Level;
        _towerLevelText.text = level.ToString();
        string nextLevelThreshold =  (level < _tower.ExperienceThresholds.Length) ? "/" + _tower.ExperienceThresholds[level].ToString() : "";
        _towerExperienceText.text = Mathf.FloorToInt(_tower.Experience.Value).ToString() + nextLevelThreshold;

        // Stats
        _towerDamageText.text = Mathf.FloorToInt(_tower.Damage.Value).ToString();
        _towerRangeText.text = Mathf.FloorToInt(_tower.Range.Value).ToString();
        _towerAttackSpeedText.text = Mathf.FloorToInt(_tower.AttackSpeed.Value).ToString();
        _towerTriggerChanceText.text = Mathf.FloorToInt(_tower.TriggerChances.Value).ToString();
        _towerCritChanceText.text = Mathf.FloorToInt(_tower.CritChance.Value).ToString();
        _towerCritDamageText.text = Mathf.FloorToInt(_tower.CritDamage.Value).ToString();
        _towerExpGainText.text = Mathf.FloorToInt(_tower.ExperienceGain.Value).ToString();
        _towerGoldGainText.text = Mathf.FloorToInt(_tower.GoldGain.Value).ToString();

        // Update tower specific constants, such as skills
        if (updateThroughly)
        {
             _towerNameText.text = _tower.Name;

            for (int i = _skillEntries.Count - 1; i >= 0; --i)
            {
                ObjectPoolManager.Instance.PoolOrDestroy(_skillEntries[i].gameObject);
                _skillEntries.RemoveAt(i);
            }

            foreach (var skill in _tower.Skills)
            {
                var go = ObjectPoolManager.Instance.Instantiate(_skillEntryPrefab.gameObject, _content.transform);
                go.transform.localScale = new Vector3(1,1,1);
                TowerSkillEntry entry = go.GetComponent<TowerSkillEntry>();

                if (entry != null)
                {
                    entry.SetEntryText(skill.Name, skill.Description, skill.Icon);
                    _skillEntries.Add(entry);
                }
            }

            _skillScrollBar.value = 1.0f;
        }
        
        if (!TowerBuilder.Instance.CanUpgrade(_tower))
        {
            _upgradeButton.interactable = false;
        }
        else
        {
            _upgradeButton.interactable = true;
        }
    }
}
