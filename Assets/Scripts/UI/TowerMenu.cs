﻿using System.IO.Pipes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerMenu : Singleton<TowerMenu>
{
    [SerializeField]
    private TowerButton _towerButtonPrefab;

    [SerializeField]
    private GameObject _content;

    private List<TowerButton> _towerButtons;

    private void Awake()
    {
        _towerButtons = new List<TowerButton>();
    }

    private void OnEnable()
    {
        UpdateTowerButtons();
        TowerBuilder.OnTowerResearched += OnTowerResearched;
        TowerBuilder.OnTowerBought += OnTowerBought;
    }

    private void OnDisable()
    {
        TowerBuilder.OnTowerResearched -= OnTowerResearched;
        TowerBuilder.OnTowerBought -= OnTowerBought;
    }

    public void OnTowerBought(Tower tower)
    {
        TowerButton towerButton = null;
        foreach(var button in _towerButtons)
        {
            if (button.Tower.name == tower.name)
            {
                towerButton = button;
                break;
            }
        }
        if (towerButton != null)
        {
            if (towerButton.Amount > 1)
            {
                towerButton.Amount -= 1;
            }
            else
            {
                _towerButtons.Remove(towerButton);
                ObjectPoolManager.Instance.PoolOrDestroy(towerButton.gameObject);
            }
        }
    }

    public void OnTowerResearched(Tower tower, int amount)
    {
        AddOrUpdateButton(tower, amount);
    }

    public void UpdateTowerButtons()
    {
        if (TowerBuilder.Instance.TowerStash != null)
        {
            foreach (KeyValuePair<Tower, int> pair in TowerBuilder.Instance.TowerStash)
            {
                AddOrUpdateButton(pair.Key, pair.Value);
            }
        }
    }

    private void AddOrUpdateButton(Tower tower, int amount)
    {
        TowerButton button = TryGetTowerButton(tower);
        if (button != null)
        {
            button.Amount = amount;
        }
        else
        {
            GameObject go = ObjectPoolManager.Instance.Instantiate(_towerButtonPrefab.gameObject, Vector3.zero, Quaternion.identity, _content.transform);
            go.transform.localScale = new Vector3(1,1,1);
            button = go.GetComponent<TowerButton>();
            if (button != null)
            {
                button.Tower = tower;
                button.Amount = amount;
                _towerButtons.Add(button);
            }
        }
    }

    private TowerButton TryGetTowerButton(Tower tower)
    {
        foreach (TowerButton button in _towerButtons)
        {
            if (button.Tower.name == tower.name)    // TODO: better comparison
            {
                return button;
            }
        }
        return null;
    }
}
