﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    [SerializeField]
    private Text _text;

    private void OnEnable()
    {
        _text.enabled = false;
        GameManager.OnGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameManager.OnGameOver -= OnGameOver;
    }

    public void OnGameOver()
    {
        _text.enabled = true;
    }
}
