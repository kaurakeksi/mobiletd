using UnityEngine;

[DisallowMultipleComponent]
public class StatEntity : MonoBehaviour
{
    [SerializeField, HideInInspector]
    private StatCollection _stats;
    public StatCollection Stats
    {
        get { return _stats; }
    }

    private StatCollection _statsCopy = new StatCollection();
    protected virtual void OnEnable()
    {
        Stats.CopyTo(_statsCopy);
    }

    protected virtual void OnDisable()
    {
        Stats.ClearAllModifiers();
        _statsCopy.CopyTo(Stats);
    }

}