﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(StatEntity))]
[RequireStat(StatNames.MovementSpeed, BaseValue=5, MinValue=1)]
public class Bullet : MonoBehaviour, ITimedUpdate
{

    private Stat _movementSpeed;
    public Stat MovementSpeed
    {
        get
        {
            if (_movementSpeed == null)
            {
                _movementSpeed = _statEntity.Stats.Get(StatNames.MovementSpeed);
            }
            return _movementSpeed;
        }
    }

    private Enemy _target;

    private Tower _source;

    private StatEntity _statEntity;

    private Vector3 _latestTargetPosition;
    private bool _initialized;

    private void Awake()
    {
        _statEntity = GetComponent<StatEntity>();
    }

    public void Initialize(Enemy target, Tower source)
    {
        _target = target;
        _source = source;
        _latestTargetPosition = _target.transform.position;
        _initialized = true;
    }

    public void OnEnable()
    {
        TimeScaler.Instance.Subscribe(this);
    }

    public void OnDisable()
    {
        TimeScaler.Instance.Unsubscribe(this);
        _initialized = false;
        _target = null;
        _source = null;
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (_initialized)
        {
            if (_target == null || _target.IsDead)
            {
                _target = null;
            }

            if (_target != null)
            {
                _latestTargetPosition = _target.transform.position;   
            }

            float delta = MovementSpeed.Value * deltaTime;
            Vector3 toTarget = _latestTargetPosition - transform.position;

            if (delta < toTarget.magnitude)
            {
                transform.position += toTarget.normalized * delta;
            }
            else
            {
                if (_target != null && _source != null)
                {
                    _target.TakeDamage(_source);
                }
                ObjectPoolManager.Instance.PoolOrDestroy(gameObject);
            }
        }
        else
        {
            ObjectPoolManager.Instance.PoolOrDestroy(gameObject);
        }
    }
}
