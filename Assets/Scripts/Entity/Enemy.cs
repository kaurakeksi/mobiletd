﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyType
{
    Normal,
    Elite,
    Mass,
    Flying,
    Boss
}

public enum EnemyRace
{
    Cube
}

[DisallowMultipleComponent]
[RequireComponent(typeof(StatEntity))]
[RequireStat(StatNames.Hitpoints, BaseValue=10, MinValue=0, MaxValue=10)]
[RequireStat(StatNames.MovementSpeed, BaseValue=3, MinValue=0)]
[RequireStat(StatNames.Armor, BaseValue=0, MinValue=0, MaxValue=100)]
[RequireStat(StatNames.Regeneration, BaseValue=1, MinValue=0)]
[RequireStat(StatNames.ExperienceOnKill, BaseValue=1, MinValue=0)]
[RequireStat(StatNames.GoldOnKill, BaseValue=1, MinValue=0)]
public class Enemy : MonoBehaviour, ITimedUpdate
{
    [SerializeField]
    private EnemyType _type;
    public EnemyType Type
    {
        get { return _type; }
        set { _type = value; }
    }

    [SerializeField]
    private EnemyRace _race;
    public EnemyRace Race
    {
        get { return _race; }
        set { _race = value; }
    }

    private Stat _hitpoints;
    public Stat Hitpoints
    {
        get
        {
            if (_hitpoints == null)
            {
                _hitpoints = StatEntity.Stats.Get(StatNames.Hitpoints);
                _hitpoints.BaseValue = _hitpoints.MaxValue;
            }
            return _hitpoints;
        }
    }

    private Stat _movementSpeed;
    public Stat MovementSpeed
    {
        get
        {
            if (_movementSpeed == null)
            {
                _movementSpeed = StatEntity.Stats.Get(StatNames.MovementSpeed);
            }
            return _movementSpeed;
        }
    }

    private Stat _armor;
    public Stat Armor
    {
        get
        {
            if (_armor == null)
            {
                _armor = StatEntity.Stats.Get(StatNames.Armor);
            }
            return _armor;
        }
    }

    private Stat _renegeneration;
    public Stat Regeneration
    {
        get
        {
            if (_renegeneration == null)
            {
                _renegeneration = StatEntity.Stats.Get(StatNames.Regeneration);
            }
            return _renegeneration;
        }
    }

    private Stat _experienceOnKill;
    public Stat ExperienceOnKill
    {
        get
        {
            if (_experienceOnKill == null)
            {
                _experienceOnKill = StatEntity.Stats.Get(StatNames.ExperienceOnKill);
            }
            return _experienceOnKill;
        }
    }

    private Stat _goldOnKill;
    public Stat GoldOnKill
    {
        get
        {
            if (_goldOnKill == null)
            {
                _goldOnKill = StatEntity.Stats.Get(StatNames.GoldOnKill);
            }
            return _goldOnKill;
        }
    }

    public float MaxLeakDamage;
    public int Wave;

    private float _positionT = 0.0f;
    public float PositionT
    {
        get { return _positionT; }
        set { _positionT = value; }
    }

    public bool IsDead
    {
        get
        {
            return Hitpoints.Value <= 0.0f || !gameObject.activeSelf;
        }
    }

    private StatEntity _statEntity;
    public StatEntity StatEntity
    {
        get
        {
            if (_statEntity == null)
            {
                _statEntity = GetComponent<StatEntity>();
            }
            return _statEntity;
        }
    }
    
    private Vector3 _baseScale;
    
    private void Awake()
    {
        _statEntity = GetComponent<StatEntity>();
        _baseScale = transform.localScale;
    }

    public void OnEnable()
    {
        _positionT = 0.0f;
        Hitpoints.BaseValue = Hitpoints.MaxValue;
        TimeScaler.Instance.Subscribe(this);
    }

    public void OnDisable()
    {
        TimeScaler.Instance.Unsubscribe(this);
        Hitpoints.BaseValue = Hitpoints.MaxValue;
        transform.localScale = _baseScale;
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (IsDead)
        {
            return;
        }

        if (!Path.Instance.MoveEnemy(this, _type == EnemyType.Flying))
        {
            Base.Instance.LeakEnemy(this);
            Die(null);
        }
    }

    public void TakeDamage(Tower source)
    {
        if (IsDead)
        {
            return;
        }

        Hitpoints.Value -= source.CalculateDamage(gameObject);

        if (IsDead)
        {
            Die(source);
        }
    }

    private void Die(Tower killer)
    {
        if (killer != null)
        {
            TowerBuilder.Instance.Gold += GoldOnKill.Value * killer.GoldGain.Value;
            killer.GainExperience(ExperienceOnKill.Value);
        }
        Hitpoints.BaseValue = -float.MaxValue;
        DeleteEnemy();
    }

    private void DeleteEnemy()
    {
        Spawner.Instance.RemoveEnemy(this);
        ObjectPoolManager.Instance.PoolOrDestroy(gameObject);
    }
}
