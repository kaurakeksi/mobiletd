﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
[RequireStat(StatNames.Hitpoints, BaseValue=100, MinValue=0, MaxValue=100)]
public class Base : StatEntity
{
    public static event System.Action OnBaseDead = () => { };

    public static Base Instance;

    private Stat _hitpoints;
    public Stat Hitpoints
    {
        get
        {
            if (_hitpoints == null)
            {
                _hitpoints = StatEntity.Stats.Get(StatNames.Hitpoints);
                _hitpoints.BaseValue = _hitpoints.MaxValue;
            }
            return _hitpoints;
        }
    }

    private StatEntity _statEntity;
    public StatEntity StatEntity
    {
        get
        {
            if (_statEntity == null)
            {
                _statEntity = GetComponent<StatEntity>();
            }
            return _statEntity;
        }
    }

    private void Awake()
    {
        Instance = this;
        _statEntity = GetComponent<StatEntity>();
    }

    public void LeakEnemy(Enemy enemy)
    {
        Hitpoints.BaseValue -= enemy.MaxLeakDamage * enemy.Hitpoints.Value / enemy.Hitpoints.MaxValue;

        if (Hitpoints.BaseValue <= 0)
        {
            OnBaseDead();
        }
    }
}
