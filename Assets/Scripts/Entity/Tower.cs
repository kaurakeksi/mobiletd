﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TowerRarity
{
    Common,
    Rare,
    Epic,
    Legendary
}

[DisallowMultipleComponent]
[RequireComponent(typeof(StatEntity))]
[RequireStat(StatNames.Damage, BaseValue=10)]
[RequireStat(StatNames.Range, BaseValue=5)]
[RequireStat(StatNames.AttackSpeed, BaseValue=1)]
[RequireStat(StatNames.CritChance, BaseValue=0.05f)]
[RequireStat(StatNames.CritDamage, BaseValue=1)]
[RequireStat(StatNames.TriggerChances, BaseValue=1)]
[RequireStat(StatNames.ExperienceGain, BaseValue=1)]
[RequireStat(StatNames.GoldGain, BaseValue=1)]
[RequireStat(StatNames.Experience, BaseValue=0, MinValue=0)]
public class Tower : MonoBehaviour, ITimedUpdate
{
    public static event System.Action<GameObject, float> OnCrit = (gameObject, damage) => { };

    [Header("UI")]
    [SerializeField]
    private string _name;
    public string Name => _name;

    [SerializeField]
    private string _description;
    public string Description => _description;

    [SerializeField]
    private Sprite _icon;
    public Sprite Icon => _icon;

    [Header("Building")]
    [SerializeField]
    private int _cost;
    public int Cost => _cost;

    [Tooltip("The earliest wave number this tower can be randomed. (For upgrades it is the first wave it can be upgraded)")]
    [SerializeField]
    private int _earliestWaveNumber;

    [Tooltip("The prefab this tower upgrades to (leave blank if none).")]
    [SerializeField]
    private Tower _upgradePrefab;
    public Tower UpgradePrefab => _upgradePrefab;

    [Header("Combat")]
    [SerializeField]
    private TargetingStyle _targetingStyle;
    public TargetingStyle TargetingStyle => _targetingStyle;

    [Header("Other")]

    [SerializeField]
    private Transform _bulletSpawn;

    [SerializeField]
    private GameObject _bulletPrefab;

    private int[] experienceThresholds = {0, 10, 25, 45, 70, 100, 135, 180, 235, 300};
    public int[] ExperienceThresholds
    {
        get
        {
            return experienceThresholds;
        }
    }
    public int Level
    {
        get
        {
            for (int i = experienceThresholds.Length - 1; i >= 0; --i)
            {
                if (Experience.Value >= experienceThresholds[i])
                {
                    return i + 1;
                }
            }
            return 1;
        }
    }

    #region Stat variables
    private Stat _damage;
    public Stat Damage
    {
        get
        {
            if (_damage == null)
            {
                _damage = _statEntity.Stats.Get(StatNames.Damage);
            }
            return _damage;
        }
    }

    private Stat _range;
    public Stat Range
    {
        get
        {
            if (_range == null)
            {
                _range = _statEntity.Stats.Get(StatNames.Range);
            }
            return _range;
        }
    }

    private Stat _attackSpeed;
    public Stat AttackSpeed
    {
        get
        {
            if (_attackSpeed == null)
            {
                _attackSpeed = _statEntity.Stats.Get(StatNames.AttackSpeed);
            }
            return _attackSpeed;
        }
    }

    private Stat _critChance;
    public Stat CritChance
    {
        get
        {
            if (_critChance == null)
            {
                _critChance = _statEntity.Stats.Get(StatNames.CritChance);
            }
            return _critChance;
        }
    }

    private Stat _critDamage;
    public Stat CritDamage
    {
        get
        {
            if (_critDamage == null)
            {
                _critDamage = _statEntity.Stats.Get(StatNames.CritDamage);
            }
            return _critDamage;
        }
    }

    private Stat _triggerChances;
    public Stat TriggerChances
    {
        get
        {
            if (_triggerChances == null)
            {
                _triggerChances = _statEntity.Stats.Get(StatNames.TriggerChances);
            }
            return _triggerChances;
        }
    }

    private Stat _experienceGain;
    public Stat ExperienceGain
    {
        get
        {
            if (_experienceGain == null)
            {
                _experienceGain = _statEntity.Stats.Get(StatNames.ExperienceGain);
            }
            return _experienceGain;
        }
    }

    private Stat _goldGain;
    public Stat GoldGain
    {
        get
        {
            if (_goldGain == null)
            {
                _goldGain = _statEntity.Stats.Get(StatNames.GoldGain);
            }
            return _goldGain;
        }
    }

    private Stat _experience;
    public Stat Experience
    {
        get
        {
            if  (_experience == null)
            {
                _experience = _statEntity.Stats.Get(StatNames.Experience);
            }
            return _experience;
        }
    }
    #endregion

    private StatEntity _statEntity;
    
    private Enemy _target;
    public Enemy Target => _target;

    private float _previousAttackTime;
    private List<Bullet> _bullets = new List<Bullet>();
    private List<Skill> _skills = new List<Skill>();
    public List<Skill> Skills => _skills;

    private void Awake()
    {
        _statEntity = GetComponent<StatEntity>();   
    }

    private void OnEnable()
    {
        TimeScaler.Instance.Subscribe(this);
    }

    private void OnDisable()
    {
        TimeScaler.Instance.Unsubscribe(this);
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        // Clear ghost bullets
        for (int i = _bullets.Count - 1; i >= 0; --i)
        {
            if (_bullets[i] == null)
            {
                _bullets.RemoveAt(i);
            }
        }

        // Update target
        if (_target == null || _target.IsDead || ((Vector2)transform.position - (Vector2)_target.transform.position).magnitude > Range.Value)
        {
            FindNewTarget();
        }
        
        // Shoot
        if (_target != null && totalTime - _previousAttackTime > AttackSpeed.Value)
        {
            _previousAttackTime = totalTime;
            Shoot();
        }

        for (int i = _skills.Count - 1; i >= 0; --i)
        {
            _skills[i].TimedUpdate(totalTime, deltaTime);
        }
    }

    public void AddSkill(Skill skill)
    {
        _skills.Add(skill);
    }

    public void RemoveSkill(Skill skill)
    {
        _skills.Remove(skill);
    }

    private void Shoot()
    {
        Bullet bullet = ObjectPoolManager.Instance.Instantiate(_bulletPrefab, _bulletSpawn.position, Quaternion.identity).GetComponent<Bullet>();


        bullet.Initialize(_target, this);
        _bullets.Add(bullet);

        for (int i = _skills.Count - 1; i >= 0; --i)
        {
            _skills[i].OnAttack();
        }
    }

    private void FindNewTarget()
    {
        var enemies = new List<Enemy>();
        foreach (var key in Spawner.Instance.Enemies.Keys)
        {
            enemies = enemies.Concat(Spawner.Instance.Enemies[key]).ToList();
        }

        _target = TargetingHelper.Instance.GetTargetEnemy(transform, _targetingStyle, Range.Value, enemies);
    }

    public void GainExperience(float experience)
    {
        Experience.BaseValue += experience * ExperienceGain.Value;

        // TODO: Add/update level stat buff.
    }

    // This is called at the time of applying damage.
    public float CalculateDamage(GameObject damageTarget)
    {
        float damage = Damage.Value;
        if (Random.value <= CritChance.Value)
        {
            damage *= CritDamage.Value;
            OnCrit(damageTarget, damage);
        }

        return damage;
    }
}
