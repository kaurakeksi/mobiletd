[System.AttributeUsage(System.AttributeTargets.Class, AllowMultiple = true)]
public class RequireStatAttribute : System.Attribute
{
    public string Name
    {
        get;
        set;
    }

    public float BaseValue
    {
        get;
        set;
    }

    public float MinValue
    {
        get;
        set;
    }

    public float MaxValue
    {
        get;
        set;
    }

    public RequireStatAttribute(string name, float baseValue = 0, float minValue = -float.MaxValue, float maxValue = float.MaxValue)
    {
        Name = name;
        BaseValue = baseValue;
        MinValue = minValue;
        MaxValue = maxValue;
    }
}
