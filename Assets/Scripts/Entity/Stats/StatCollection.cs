using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public sealed class StatCollection : IEnumerable<Stat>
{
    [SerializeField]
    private List<Stat> _stats = new List<Stat>();

    public Stat Add(string name)
    {
        var stat = Get(name);
        if (stat != null)
        {
            return stat;
        }

        stat = new Stat(name);
        _stats.Add(stat);
        return stat;
    }

    public void Remove(string name)
    {
        var stat = Get(name);
        _stats.Remove(stat);
    }

    public Stat Get(string name)
    {
        foreach (var stat in _stats)
        {
            if (stat.Name == name)
            {
                return stat;
            }
        }
        return null;
    }

    public bool Contains(string name)
    {
        foreach (var stat in _stats)
        {
            if (stat.Name == name)
            {
                return true;
            }
        }
        return false;
    }

    public void ClearAllModifiers()
    {
        foreach (var stat in _stats)
        {
            stat.ClearModifiers();
        }
    }

    public IEnumerator<Stat> GetEnumerator()
    {
        return _stats.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void CopyTo(StatCollection other)
    {
        for (var i = other._stats.Count - 1; i >= 0; --i)
        {
            var name = other._stats[i].Name;
            if (!Contains(name))
            {
                other.Remove(name);
            }
        }

        foreach (var stat in _stats)
        {
            var statOther = other.Get(stat.Name) ?? other.Add(stat.Name);
            statOther.MinValue = stat.MinValue;
            statOther.MaxValue = stat.MaxValue;
            statOther.BaseValue = stat.BaseValue;
        }
    }
}
