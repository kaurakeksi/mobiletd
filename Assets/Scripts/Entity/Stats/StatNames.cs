public static class StatNames
{
    // Enemy
    public const string Hitpoints = "Hitpoints";
    public const string MovementSpeed = "Movement speed";
    public const string ExperienceOnKill = "Experience on kill";
    public const string GoldOnKill = "Gold on kill";
    public const string Armor = "Armor";
    public const string Regeneration = "Hitpoint Regeneration";


    // Tower

    public const string Damage = "Damage";
    public const string CritChance = "Critical chance";
    public const string CritDamage = "Critical damage";

    public const string SpellDamage = "Spell damage";
    public const string SpellCritChance = "Spell critical chance";
    public const string SpellCritDamage = "Spell critical damage";


    public const string AttackSpeed = "Attack speed";
    public const string Range = "Range";
    
    public const string TriggerChances = "Trigger chances";
    public const string Experience = "Experience";
    public const string ExperienceGain = "Experience gain";
    public const string GoldGain = "Gold gain";
}