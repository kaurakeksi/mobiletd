using UnityEngine;

public enum StatModifierOrder
{
    Early = 25,
    Default = 50,
    Late = 75
}

public enum StatModifierType
{
    FlatAdd,
    PercentAdd,
    PercentMultiply
}

// Data needed for modifying a Stat.
[System.Serializable]
public sealed class StatModifier
{
    [SerializeField]
    [Tooltip("Value to modify the Stat with.")]
    private float _value = 0.5f;
    public float Value
    {
        get { return _value; }
    }

    [SerializeField]
    [Tooltip("Modifiers with lower order are applied first.")]
    private int _order = (int)StatModifierOrder.Default;
    public int Order
    {
        get { return _order; }
    }

    [SerializeField]
    [Tooltip("How should the StatModifier affect the Stat.")]
    private StatModifierType _type = StatModifierType.PercentMultiply;
    public StatModifierType Type
    {
        get { return _type; }
    }

    public StatModifier(float value, int order = (int)StatModifierOrder.Default)
        : this(value, StatModifierType.PercentMultiply, order)
    {
    }

    public StatModifier(float value, StatModifierOrder order)
        : this(value, StatModifierType.PercentMultiply, order)
    {
    }

    public StatModifier(float value, StatModifierType type, int order = (int)StatModifierOrder.Default)
    {
        _value = value;
        _type = type;
        _order = order;
    }

    public StatModifier(float value, StatModifierType type, StatModifierOrder order)
    {
        _value = value;
        _type = type;
        _order = (int)order;
    }

    public int CompareTo(object obj)
    {
        if (obj == null)
        {
            return 1;
        }

        var other = obj as StatModifier;
        if (other == null)
        {
            throw new System.ArgumentException("Object is not a Stat");
        }

        var result = Order.CompareTo(other.Order);
        if (result != 0)
        {
            return result;
        }

        result = Type.CompareTo(other.Type);
        if (result != 0)
        {
            return result;
        }

        result = Value.CompareTo(other.Value);
        return result;
    }
}
