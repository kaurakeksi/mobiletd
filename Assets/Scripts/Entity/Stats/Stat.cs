using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public sealed class Stat
{
    private bool _isClean;

    private List<StatModifier> _modifiers;
    public List<StatModifier> Modifiers
    {
        get
        {
            if (_modifiers == null)
            {
                _modifiers = new List<StatModifier>();
            }
            return _modifiers;
        }

    }

    [SerializeField]
    public string _name;
    public string Name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private float _baseValue;
    public float BaseValue
    {
        get { return _baseValue; }
        set
        {
            var val = Mathf.Clamp(value, MinValue, MaxValue);
            if (_baseValue != val)
            {
                _baseValue = val;
                _isClean = false;
            }
        }
    }

    [SerializeField]
    private float _minValue;
    public float MinValue
    {
        get { return _minValue; }
        set
        {
            _minValue = value;
            BaseValue = _baseValue;
        }
    }

    [SerializeField]
    private float _maxValue;
    public float MaxValue
    {
        get { return _maxValue; }
        set
        {
            _maxValue = value;
            BaseValue = _baseValue;
        }
    }

    private float _value;
    public float Value
    {
        get
        {
            if (!_isClean)
            {
                _value = ApplyModifiers(BaseValue);
                _isClean = true;
            }
            return _value;
        }
        set
        {
            BaseValue = StripModifiers(value);
        }
    }

    public Stat(string name)
    {
        Name = name;
    }

    public void AddModifier(StatModifier modifier)
    {
        if (!Modifiers.Contains(modifier))
        {
            Modifiers.Add(modifier);
            Modifiers.Sort((x, y) => x.CompareTo(y));
            _isClean = false;
        }
    }

    public void RemoveModifier(StatModifier modifier)
    {
        if (Modifiers.Remove(modifier))
        {
            _isClean = false;
        }
    }

    public void ClearModifiers()
    {
        if (Modifiers.Count > 0)
        {
            Modifiers.Clear();
            _isClean = false;
        }
    }

    public float ApplyModifiers(float value)
    {
        var sumPercentAdd = 0.0f;
        for (var i = 0; i < Modifiers.Count; ++i)
        {
            var modifier = Modifiers[i];
            switch(modifier.Type)
            {
                case StatModifierType.FlatAdd:
                    value += modifier.Value;
                    break;
                case StatModifierType.PercentAdd:
                    sumPercentAdd += modifier.Value;
                    if (i == Modifiers.Count || Modifiers[i+1].Type != StatModifierType.PercentAdd)
                    {
                        value *= sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                    break;
                case StatModifierType.PercentMultiply:
                    value *= modifier.Value;
                    break;
            }
        }
        return value;
    }

    public float StripModifiers(float value)
    {
        var sumPercentAdd = 0.0f;
        for (var i = Modifiers.Count - 1; i >= 0; --i)
        {
            var modifier = Modifiers[i];
            switch(modifier.Type)
            {
                case StatModifierType.FlatAdd:
                    value -= modifier.Value;
                    break;
                case StatModifierType.PercentAdd:
                    sumPercentAdd += modifier.Value;
                    if (i == 0 || Modifiers[i-1].Type != StatModifierType.PercentAdd)
                    {
                        value /= sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                    break;
                case StatModifierType.PercentMultiply:
                    value /= modifier.Value;
                    break;
            }
        }
        return value;
    }
}
