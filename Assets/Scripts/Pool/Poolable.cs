using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
public sealed class Poolable : MonoBehaviour
{
    public ObjectPool ObjectPool
    {
        get;
        private set;
    }

    public bool IsPooled
    {
        get;
        private set;
    }

    public void OnUnpool(ObjectPool pool)
    {
        ObjectPool = pool;
        IsPooled = false;
    }

    public void OnPool(ObjectPool pool)
    {
        Debug.Assert(!IsPooled);

        ObjectPool = pool;
        IsPooled = true;
    }
}
