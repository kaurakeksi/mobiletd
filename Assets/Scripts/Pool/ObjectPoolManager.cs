using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public sealed class ObjectPoolManager : Singleton<ObjectPoolManager>
{
    // GameObject under which active instanced GameObjects reside by default.
    [SerializeField]
    private GameObject _activeParentObject;
    public GameObject ActiveParentObject => _activeParentObject;

    // GameObject under which all inactive pooled GameObjects reside.
    [SerializeField]
    private GameObject _pooledParentObject;
    public GameObject PooledParentObject => _pooledParentObject;

    public static bool IsEnabled
    {
        get { return Instance.gameObject.activeInHierarchy && Instance.enabled; }
        set { Instance.enabled = value; }
    }

    private static readonly Dictionary<GameObject, ObjectPool> _pools;

    static ObjectPoolManager()
    {
        _pools = new Dictionary<GameObject, ObjectPool>();
    }

    private void OnDisable()
    {
        ClearPools();
    }

    public GameObject Instantiate(GameObject prefab, Transform parent, bool active = true)
    {
        return Instantiate(prefab, Vector3.zero, Quaternion.identity, parent, active);
    }

    public GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null, bool active = true)
    {
        if (!IsEnabled)
        {
            return Object.Instantiate(prefab, position, rotation, parent);
        }

        // If no Poolable component is available, do a normal Instantiate.
        var poolable = prefab.GetComponent<Poolable>();
        if (poolable == null)
        {
            return Object.Instantiate(prefab, position, rotation, parent);
        }

        // Get the pool for the given prefab or create a new one if it doesn't exist.
        var pool = GetOrCreatePool(prefab);

        // If pool is empty, create a new GameObject.
        if (pool.IsEmpty)
        {
            return pool.Instantiate(prefab, position, rotation, parent ?? ActiveParentObject.transform, active);
        }
        return pool.Get(position, rotation, parent ?? ActiveParentObject.transform, active);
    }

    public void ResizeObjectPool(GameObject prefab, int size)
    {
        var pool = GetOrCreatePool(prefab);
        pool.Resize(size);
    }

    public void PoolOrDestroy(GameObject obj)
    {
        if (obj == null)
        {
            return;
        }

        if (!IsEnabled)
        {
            Object.Destroy(obj);
            return;
        }

        var poolable = obj.GetComponent<Poolable>();
        if (poolable == null || poolable.ObjectPool == null || !poolable.ObjectPool.Pool(obj))
        {
            Destroy(obj);
        }
        else
        {
            poolable.transform.SetParent(PooledParentObject.transform);
        }
    }

    public void ClearPools()
    {
        foreach (var pool in _pools.Values)
        {
            pool.Clear();
        }
        _pools.Clear();
    }

    private ObjectPool GetOrCreatePool(GameObject prefab)
    {
        ObjectPool pool;
        if (!_pools.TryGetValue(prefab, out pool))
        {
            pool = new ObjectPool(prefab, 1, 300);
            _pools.Add(prefab, pool);
        }
        return pool;
    }
}