using System.Collections.Generic;
using UnityEngine;

public sealed class ObjectPool
{
    private int _maxSize;
    private readonly Queue<GameObject> _pool;

    public bool IsEmpty
    {
        get { return _pool.Count == 0; }
    }

    public ObjectPool(int maxSize = 100)
    {
        _pool = new Queue<GameObject>();
        _maxSize = maxSize;
    }

    public ObjectPool(GameObject prefab, int initialSize, int maxSize = 100)
        : this(maxSize)
    {
        for (var i = 0; i < initialSize; ++i)
        {
            var obj = Instantiate(prefab, false);
            Pool(obj);
        }
    }

    // Destroys all GameObjects in the pool.
    public void Clear()
    {
        foreach (var obj in _pool)
        {
            if (obj != null)
            {
                Object.Destroy(obj);
            }
        }
        _pool.Clear();
    }

    // Gets an existing instance from the pool.
    public GameObject Get(Vector3 position, Quaternion rotation, Transform parent, bool active = true)
    {
        var obj = _pool.Dequeue();
        obj.transform.SetParent(parent);
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        obj.SetActive(active);

        // Make sure the object can be repooled.
        var poolable = obj.GetComponent<Poolable>();
        if (poolable)
        {
            poolable.OnUnpool(this);
        }
        return obj;
    }

    // Creates a new instance that can be pooled.
    public GameObject Instantiate(GameObject prefab, bool active = true)
    {
        return Instantiate(prefab, Vector3.zero, Quaternion.identity, null, active);
    }

    // Creates a new instance that can be pooled.
    public GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent, bool active = true)
    {
        if (prefab.activeSelf)
        {
            Debug.LogError("Pooled prefabs need to inactive to make sure Awake() is called before OnEnable(). Deactivating prefab " + prefab.name);
            //Debug.LogError("NOTE: AUTOMATIC DEACTIVATING DISABLED FOR NOW");
            prefab.SetActive(false);
        }

        var obj = Object.Instantiate(prefab, position, rotation, parent);
        obj.SetActive(active);

        // Make sure the object can be repooled.
        var poolable = obj.GetComponent<Poolable>();
        if (poolable)
        {
            poolable.OnUnpool(this);
        }
        return obj;
    }

    // Adds the given GameObject to the pool.
    public bool Pool(GameObject obj)
    {
        // Check if pool if full.
        if (_pool.Count >= _maxSize)
        {
            Debug.LogWarning("Failed to pool " + obj + ": Pool full.");
            return false;
        }

        var poolable = obj.GetComponent<Poolable>();
        if (poolable == null)
        {
            Debug.LogWarning("Failed to pool " + obj + ": No Poolable attached.");
            return false;
        }

        if (poolable.IsPooled)
        {
            Debug.LogWarning("Failed to pool " + obj + ": Already pooled.");
            return false;
        }

        // Make sure double pooling can't happen.
        poolable.OnPool(this);
        obj.SetActive(false);
        _pool.Enqueue(obj);
        return true;
    }

    // Resizes the pools maximum capacity to the given size.
    public void Resize(int size)
    {
        _maxSize = Mathf.Max(0, size);

        // Remove overflowing instances.
        while (_pool.Count > _maxSize)
        {
            var obj = _pool.Dequeue();
            Object.Destroy(obj);
        }
    }
}
