using UnityEngine;

public interface ITimedUpdate
{
    GameObject gameObject { get; }
    
    // NOTE! Must also call TimeScaler.Instance.Subscribe() in OnEnable()
    // and Unsubscribe() in OnDisable() for TimedUpdate to be called!!
    void TimedUpdate(float totalTime, float deltaTime);
}