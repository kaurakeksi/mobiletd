﻿using System.Runtime.InteropServices.ComTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PathSegment
{
    public PathSegment(Vector3 start, Vector3 end, float length)
    {
        Start = start;
        End = end;
        Length = length;
    }
    public Vector3 Start;
    public Vector3 End;
    public float Length;
    public float UnclampedT(Vector3 position)
    {
        // Shift the problem to the origin to simplify the math.    
        var wander = position - Start;
        var span = End - Start;

        // Compute how far along the line is the closest approach to our position.
        float t = Vector3.Dot(wander, span) / span.sqrMagnitude;

        return t;
    }
}

public class Path : MonoBehaviour
{
    public static Path Instance;

    private Transform[] _waypoints;
    private PathSegment[] _path;
    private float _pathLength;
    private PathSegment _flyingSegment;

    private void Awake()
    {
        Instance = this;

        _waypoints = new Transform[transform.childCount];
        for (int i = 0; i < _waypoints.Length; i++)
        {
            _waypoints[i] = transform.GetChild(i);
        }

        if (_waypoints.Length < 2)
        {
            Debug.LogError("Path must have at least 2 waypoints.");
        }

        _path = new PathSegment[_waypoints.Length - 1];
        _pathLength = 0.0f;
        for (int i = 0; i < _waypoints.Length - 1; i++)
        {
            Vector2 start = _waypoints[i].position;
            Vector2 end = _waypoints[i + 1].position;
            float length = (end - start).magnitude;
            _path[i] = new PathSegment(start, end, length);
            _pathLength += length;
        }

        Vector2 flyingStart = _waypoints[0].position;
        Vector2 flyingEnd = _waypoints[_waypoints.Length - 1].position;
        float flyingLength = (flyingEnd - flyingStart).magnitude;
        _flyingSegment = new PathSegment(flyingStart, flyingEnd, flyingLength);
    }

    // Returns false if enemy reaches the end of the path
    // Otherwise returns true
    public bool MoveEnemy(Enemy enemy, bool flying)
    {
        if (!flying)
        {
            float deltaT = enemy.MovementSpeed.Value * Time.deltaTime / _pathLength;
            float accumulatingLength = 0.0f;
            foreach (PathSegment segment in _path)
            {
                float enemyPositionLength = enemy.PositionT * _pathLength;
                float distanceToMove = deltaT * _pathLength;
                if (accumulatingLength + segment.Length > enemyPositionLength + distanceToMove)
                {
                    float lengthOverflow = enemyPositionLength - accumulatingLength;
                    enemy.transform.position = Vector3.Lerp(segment.Start, segment.End, (lengthOverflow + distanceToMove) / segment.Length);
                    enemy.PositionT += deltaT;
                    return true;
                }
                accumulatingLength += segment.Length;
            }
        }
        else
        {
            float deltaT = enemy.MovementSpeed.Value * Time.deltaTime / _flyingSegment.Length;
            if (1.0f - enemy.PositionT > deltaT)
            {
                enemy.PositionT += deltaT;
                enemy.transform.position = (Vector3.Lerp(_flyingSegment.Start, _flyingSegment.End, enemy.PositionT));
                return true;
            }

            enemy.transform.position = _flyingSegment.End;
            return false;
        }
        enemy.transform.position = _path[_path.Length - 1].End;
        return false;
    }
}
