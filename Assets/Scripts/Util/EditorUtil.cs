using System.Reflection;
using UnityEditor;
using UnityEngine;

public static class EditorUtil
{
    public static void DrawHeader(string name)
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField(name, EditorStyles.boldLabel);
    }

    public static void DrawProperty(string name, SerializedObject serializedObject)
    {
        var property = serializedObject.FindProperty(name);
        if (property != null)
        {
            EditorGUILayout.PropertyField(property, true);
        }
        else
        {
            Debug.LogError(string.Format("No property '{0}' found.", name));
        }
    }

    public static object GetTargetObjectOfProperty(SerializedProperty property)
    {
        var path = property.propertyPath.Replace(".Array.data[", "[");
        object obj = property.serializedObject.targetObject;
        var elements = path.Split('.');
        foreach (var element in elements)
        {
            if (element.Contains("["))
            {
                var elementName = element.Substring(0, element.IndexOf("[", System.StringComparison.Ordinal));
                var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[", System.StringComparison.Ordinal)).Replace("[", "").Replace("]", ""));
                obj = GetValue_Imp(obj, elementName, index);
            }
            else
            {
                obj = GetValue_Imp(obj, element);
            }
        }
        return obj;
    }

    private static object GetValue_Imp(object source, string name)
    {
        if (source == null)
        {
            return null;
        }

        var type = source.GetType();
        while (type != null)
        {
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f != null)
            {
                return f.GetValue(source);
            }

            var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
            if (p != null)
            {
                return p.GetValue(source, null);
            }

            type = type.BaseType;
        }

        return null;
    }

    private static object GetValue_Imp(object source, string name, int index)
    {
        var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
        if (enumerable == null)
        {
            return null;
        }

        var enm = enumerable.GetEnumerator();
        for (int i = 0; i <= index; i++)
        {
            if (!enm.MoveNext())
            {
                return null;
            }
        }

        return enm.Current;
    }
}
