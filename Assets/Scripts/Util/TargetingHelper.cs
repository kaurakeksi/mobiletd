using UnityEngine;
using System.Collections.Generic;

public enum TargetingStyle
{
    First,
    Nearest
}

public class TargetingHelper : Singleton<TargetingHelper>
{
    public Enemy GetTargetEnemy(Transform caller, TargetingStyle targetingStyle, float callerRange, List<Enemy> candidates)
    {
        // Square range so we can use distance^2 to avoid sqrt.
        float rangeSquared = callerRange * callerRange;

        Enemy result = null;
        if (targetingStyle == TargetingStyle.First)
        {
            float maxT = -float.MaxValue;
            foreach (Enemy enemy in candidates)
            {
                if (enemy.IsDead)
                {
                    continue;
                }

                float tempDistance = distance2DSquared(caller.position, enemy.transform.position);
                if (tempDistance <= rangeSquared && enemy.PositionT > maxT)
                {
                    maxT = enemy.PositionT;
                    result = enemy;
                }
            }
        }
        else if (targetingStyle == TargetingStyle.Nearest)
        {
            float minDistance = float.MaxValue;
            foreach (Enemy enemy in candidates)
            {
                if (enemy.IsDead)
                {
                    continue;
                }
                
                float tempDistance = distance2DSquared(caller.position, enemy.transform.position);
                if (tempDistance < minDistance && tempDistance <= rangeSquared)
                {
                    result = enemy;
                    minDistance = tempDistance;
                }
            }
        }
        return result;
    }

    private static float distance2DSquared(Vector2 position, Vector2 other)
    {
        return (position - other).sqrMagnitude;
    }
}