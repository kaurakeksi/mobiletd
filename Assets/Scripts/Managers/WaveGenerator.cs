using System.Collections.Generic;
using UnityEngine;

public struct EnemyData
{
    public int Hitpoints;   // TODO: Use something else instead? This WILL overflow!
    public EnemyType Type;
    public EnemyRace Race;
    public float LocalScaleMultiplier;
    public float Experience;
    public float Gold;
    public float MaxLeakDamage;
}

public struct Wave
{
    public int WaveNumber;
    public List<EnemyData> Enemies;
    public Wave(int waveNumber)
    {
        WaveNumber = waveNumber;
        Enemies = new List<EnemyData>();
    }
}

public class WaveGenerator : Singleton<WaveGenerator>
{
    [Header("Generation")]
    [Tooltip("The wave number after which waves spawn the maximum number of enemies.")]
    [SerializeField]
    private int _monsterCountCapWave;

    [Tooltip("Prototypes that define possible wave enemy structures.")]
    [SerializeField]
    private List<WavePrototype> _wavePrototypes;

    [Header("Internal")]
    [Tooltip("Determines if specified seed is used to initialize random number generator.")]
    [SerializeField]
    private bool _useSeed;

    [Tooltip("Seed to initialize random number generator used solely for wave generation.")]
    [SerializeField]
    private int _seed;

    [Tooltip("How large buffer of future wave information should be kept.")]
    [SerializeField]
    private int _waveBufferSize;

    private List<Wave> _waveBuffer;
    private System.Random _random;

    private void Awake()
    {
        _waveBuffer = new List<Wave>();
        if (_useSeed)
        {
            _random = new System.Random(_seed);
        }
        else
        {
            _random = new System.Random();
        }
    }

    public Wave GetWave(int waveNumber)
    {
        if (_waveBufferSize > _waveBuffer.Count - waveNumber)
        {
            for (int i = _waveBuffer.Count + 1; i < waveNumber + _waveBufferSize; ++i)
            {
                _waveBuffer.Add(GenerateWave(i));
            }
        }

        return _waveBuffer[waveNumber - 1];
    }

    private Wave GenerateWave(int waveNumber)
    {
        Wave result = new Wave(waveNumber);

        // Randomize wave prototype
        WavePrototype wavePrototype = _wavePrototypes[_random.Next(0, _wavePrototypes.Count)];
        var prototypeMinMax = WavePrototypeMinMax(wavePrototype);

        // Adjust the amount of enemies by current wave
        int totalEnemyCount = 0;
        Dictionary<EnemyType, int> waveAdjustedCounts = new Dictionary<EnemyType, int>();
        foreach (var key in prototypeMinMax.Keys)
        {
            var min = prototypeMinMax[key].Item1;
            var max = prototypeMinMax[key].Item2;

            if (!waveAdjustedCounts.ContainsKey(key))
            {
                waveAdjustedCounts.Add(key, 0);
            }
            waveAdjustedCounts[key] = (int) (min + (max - min) * Mathf.Min((float) waveNumber / _monsterCountCapWave, 1.0f));
            totalEnemyCount += waveAdjustedCounts[key];
        }

        // Randomize enemy properties
        var raceArray = System.Enum.GetValues(typeof(EnemyRace));
        EnemyRace race = (EnemyRace) raceArray.GetValue(_random.Next(0, raceArray.Length));

        Dictionary<EnemyType, int> spawned = new Dictionary<EnemyType, int>();
        foreach (var subWave in wavePrototype.SubWaves)
        {
            EnemyType type = subWave.EnemyType;
            int count = Mathf.Min(waveAdjustedCounts[type], subWave.MaxEnemyCount);
            waveAdjustedCounts[type] -= count;

            if (!spawned.ContainsKey(type))
            {
                spawned.Add(type, 0);
            }
            spawned[type] += count;


            // TODO: EnemyType specific data should be defined somewhere somewhere else (scriptable object?)
            for (int i = 0; i < count; ++i)
            {
                EnemyData enemyData = new EnemyData();
                enemyData.Race = race;
                enemyData.Type = type;

                // Hitpoints scaling
                float baseHitpointsMultiplier = 1.0f;
                if (type == EnemyType.Elite)
                {
                    baseHitpointsMultiplier = 0.15f;
                }
                else if (type == EnemyType.Flying)
                {
                    baseHitpointsMultiplier = 0.15f;
                }
                else if (type == EnemyType.Normal)
                {
                    baseHitpointsMultiplier = 0.09f;
                }
                else if (type == EnemyType.Mass)
                {
                    baseHitpointsMultiplier = 0.045f;
                }
                enemyData.Hitpoints = (int) (GameManager.Instance.EnemyBaseHealth * baseHitpointsMultiplier * Mathf.Pow(1.08f, (waveNumber - 1)));

                // TODO: elites should give more exp than normals/mass when in the same wave
                // Experience scaling
                enemyData.Experience = GameManager.Instance.WaveBaseExp / (float) totalEnemyCount;

                // TODO: elites should give more gold than normals/mass when in the same wave
                // Gold scaling
                enemyData.Gold = (GameManager.Instance.WaveBaseGold + 2.0f * waveNumber) / totalEnemyCount;

                // TODO: elites should do more damage than normals/mass when in the same wave
                // Base damage scaling
                enemyData.MaxLeakDamage = GameManager.Instance.WaveLeakBaseDamage / (float) totalEnemyCount;

                // Size scale
                if (type == EnemyType.Elite)
                {
                    enemyData.LocalScaleMultiplier = 2.0f;
                }
                else if (type == EnemyType.Flying)
                {
                    enemyData.LocalScaleMultiplier = 2.2f;
                }
                else if (type == EnemyType.Boss)
                {
                    enemyData.LocalScaleMultiplier = 3.6f;
                }
                else if (type == EnemyType.Mass)
                {
                    enemyData.LocalScaleMultiplier = 0.5f;
                }
                else
                {
                    enemyData.LocalScaleMultiplier = 1.0f;
                }

                result.Enemies.Add(enemyData);
            }
        }

        return result;
    }

    private Dictionary<EnemyType, System.Tuple<int,int>> WavePrototypeMinMax(WavePrototype prototype)
    {
        Dictionary<EnemyType, System.Tuple<int,int>> result = new Dictionary<EnemyType, System.Tuple<int,int>>();

        Dictionary<EnemyType, int> minCounts = new Dictionary<EnemyType, int>();
        Dictionary<EnemyType, int> maxCounts = new Dictionary<EnemyType, int>();
        foreach (var subWave in prototype.SubWaves)
        {
            if (!minCounts.ContainsKey(subWave.EnemyType))
            {
                minCounts.Add(subWave.EnemyType, 0);
            }
            if (!maxCounts.ContainsKey(subWave.EnemyType))
            {
                maxCounts.Add(subWave.EnemyType, 0);
            }

            minCounts[subWave.EnemyType] += subWave.MinEnemyCount;
            maxCounts[subWave.EnemyType] += subWave.MaxEnemyCount;
        }

        foreach (var key in minCounts.Keys)
        {
            result[key] = new System.Tuple<int, int>(minCounts[key], maxCounts[key]);
        }

        return result;
    }
}