﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TowerBuilder : Singleton<TowerBuilder>
{
    public static event System.Action<Tower, int> OnTowerResearched = (tower, amount) => { };
    public static event System.Action<Tower> OnTowerBought = (tower) => { };

    [Header("UI")]
    [SerializeField]
    private GameObject _towerMenuUI;

    [SerializeField]
    private TowerInfoUI _towerInfoUI;

    [Header("Tilemap")]
    [Tooltip("Tilemap containing buildable tiles")]
    [SerializeField]
    private Tilemap _tilemap;

    [SerializeField]
    private Color _tileSelectionColor;

    [SerializeField]
    private GameObject _towerPrefab;

    [SerializeField]
    private List<Tower> _towerPool;

    private float _gold;
    public float Gold
    {
        get { return _gold; }
        set
        {
            _gold = value;
            Debug.Assert(_gold >= 0.0f);
        }
    }

    private Dictionary<Tower, int> _towerStash;
    public Dictionary<Tower, int> TowerStash => _towerStash;

    private Dictionary<Vector3Int, Tower> _towers;

    private Vector3Int _currentTileSelection;
    private Color _selectionOriginalColor;
    private bool _isSelection = false;
    private bool _nonUIPress;

    private Camera _camera;

    private void Awake()
    {
        _camera = Camera.main;
        _towerStash = new Dictionary<Tower, int>();
        _towers = new Dictionary<Vector3Int, Tower>();
    }

    private void OnEnable()
    {
        _towerInfoUI.gameObject.SetActive(false);
        _towerMenuUI.SetActive(false);
        _isSelection = false;
        _nonUIPress = false;

        InputHandler.OnScreenHover += OnScreenHover;
        InputHandler.OnScreenPress += OnScreenPress;
        InputHandler.OnScreenRelease += OnScreenRelease;
        Spawner.OnWaveEnded += OnWaveEnded;
        TowerButton.OnTowerButtonPressed += OnTowerButtonPressed;
        GameManager.OnGameOver += OnGameOver;
        TowerInfoUI.OnTowerUpgradeButtonPressed += OnTowerUpgradeButtonPressed;

        for (int i = 0; i < GameManager.Instance.StartingTowers; ++i)
        {
            RandomizeTower(0);
        }
        Gold = GameManager.Instance.StartingGold;
    }

    private void OnDisable()
    {
        InputHandler.OnScreenHover -= OnScreenHover;
        InputHandler.OnScreenPress -= OnScreenPress;
        InputHandler.OnScreenRelease -= OnScreenRelease;
        Spawner.OnWaveEnded -= OnWaveEnded;
        TowerButton.OnTowerButtonPressed -= OnTowerButtonPressed;
        GameManager.OnGameOver -= OnGameOver;
        TowerInfoUI.OnTowerUpgradeButtonPressed += OnTowerUpgradeButtonPressed;
    }

    public void OnTowerUpgradeButtonPressed(Tower tower)
    {
        if (!CanUpgrade(tower))
        {
            return;
        }

        var key = new Vector3Int(_currentTileSelection.x, _currentTileSelection.y, 0);

        _towers.Remove(key);
        BuildTower(tower.UpgradePrefab);

        // Keep everything non-tower specific stats, such as experience.
        if (_towers.ContainsKey(key))
        {
            _towers[key].Experience.BaseValue = tower.Experience.BaseValue;
        }

        _towerInfoUI.SetInfo(_towers[key]);
        ObjectPoolManager.Instance.PoolOrDestroy(tower.gameObject);
    }

    public bool CanUpgrade(Tower tower)
    {
        var key = new Vector3Int(_currentTileSelection.x, _currentTileSelection.y, 0);
        return _isSelection && tower != null && tower.UpgradePrefab != null && tower.UpgradePrefab.Cost <= Gold &&
            _towers.Keys.Contains(key) && _towers[key] == tower;
    }

    public void OnTowerButtonPressed(TowerButton button, Tower tower)
    {
        if (_towerStash.ContainsKey(tower) && tower.Cost <= Gold && _isSelection && tower != null)
        {
            Tower previousTower = null;
            Vector3Int key = (new Vector3Int(_currentTileSelection.x, _currentTileSelection.y, 0));
            if (_towers.ContainsKey(key))
            {
                previousTower = _towers[key];
                _towers.Remove(key);
            }
            BuildTower(tower);

            if (previousTower != null)
            {
                _towers[key].Experience.BaseValue = previousTower.Experience.BaseValue;
                ObjectPoolManager.Instance.PoolOrDestroy(previousTower.gameObject);
            }

            // Remove tower from stash
            if (_towerStash[tower] > 1)
            {
                _towerStash[tower] -= 1;
            }
            else
            {
                _towerStash.Remove(tower);
            }

            RevertTileSelection();
            _towerMenuUI.SetActive(false);

            OnTowerBought(tower);
        }
    }

    public void OnWaveEnded(int wave)
    {
        if (GameManager.Instance.GameOver)
        {
            return;
        }

        RandomizeTower(wave);

        // TODO: Give income here for now. This should probably be moved to a better location
        // along with rest of the gold logic.
        Gold += GameManager.Instance.Income + 2 * wave;
    }

    public void OnGameOver()
    {
        _nonUIPress = false;
        _towerInfoUI.gameObject.SetActive(false);
        _towerMenuUI.SetActive(false);
        RevertTileSelection();
    }

    // TODO: actual logic here instead of picking random tower
    private void RandomizeTower(int wave)
    {
        Tower tower = _towerPool[Random.Range(0, _towerPool.Count)];

        if (!_towerStash.Keys.Contains(tower))
        {
            _towerStash.Add(tower, 1);
        }
        else
        {
            _towerStash[tower] += 1;
        }

        if (wave > 0)
        {
            OnTowerResearched(tower, _towerStash[tower]);
        }
    }

    private void BuildTower(Tower prefab)
    {
        Vector3Int selection = new Vector3Int(_currentTileSelection.x, _currentTileSelection.y, 0);
        Vector3 position = _tilemap.CellToWorld(_currentTileSelection);
        GameObject go = ObjectPoolManager.Instance.Instantiate(prefab.gameObject, position, Quaternion.identity);
        Tower tower = go.GetComponent<Tower>();

        if (tower != null)
        {
            Gold -= prefab.Cost;
            _towers.Add(selection, tower);
        }
    }

    #region Tilemap logic
    private void SelectTile(Vector3Int tilemapPoint)
    {
        _selectionOriginalColor = _tilemap.GetColor(tilemapPoint);
        _currentTileSelection = tilemapPoint;

        _tilemap.SetTileFlags(tilemapPoint, TileFlags.None);
        _tilemap.SetColor(tilemapPoint, _tileSelectionColor);

        _isSelection = true;
    }

    private void RevertTileSelection()
    {
        if (_isSelection && _tilemap.HasTile(_currentTileSelection))
        {
            _tilemap.SetColor(_currentTileSelection, _selectionOriginalColor);
            _isSelection = false;
        }
    }

    private Vector3Int ScreenToTilemapPos(Vector2 screenPosition)
    {
        // For some reason _camera.ScreenToWorldPoint doesn't return correct point
        // so this hack will do for now
        Ray ray = _camera.ScreenPointToRay(screenPosition);
        var plane = new Plane(Vector3.back, Vector3.zero);
        float hitDist;
        plane.Raycast(ray, out hitDist);
        Vector3 hitPoint = ray.GetPoint(hitDist);

        return _tilemap.WorldToCell(hitPoint);
    }
    #endregion

    #region  Input logic
    public void OnScreenHover(Vector2 screenPosition)
    {
        if (GameManager.Instance.GameOver || InputHandler.Instance.IsUIAtScreenPosition(screenPosition))
        {
            return;
        }
        
        if (_nonUIPress)
        {
            RevertTileSelection();
            Vector3Int tilemapPoint = ScreenToTilemapPos(screenPosition);
            if (_tilemap.HasTile(tilemapPoint))
            {
                SelectTile(tilemapPoint);
            }
        }
    }

    public void OnScreenRelease(Vector2 screenPosition)
    {
        if (GameManager.Instance.GameOver)
        {
            _towerInfoUI.gameObject.SetActive(false);
            _towerMenuUI.SetActive(false);
            RevertTileSelection();
            return;
        }

        if (_nonUIPress)
        {
            if (!InputHandler.Instance.IsUIAtScreenPosition(screenPosition))
            {
                if (_isSelection && _tilemap.HasTile(_currentTileSelection))
                {
                    // If the selected tile already contains tower, show the tower's info.
                    // Otherwise open tower building menu.
                    Vector3Int z0Selection = new Vector3Int(_currentTileSelection.x, _currentTileSelection.y, 0);
                    if (_towers.ContainsKey(z0Selection))
                    {
                        _towerInfoUI.SetInfo(_towers[z0Selection]);
                        _towerInfoUI.gameObject.SetActive(true);
                        _towerMenuUI.SetActive(true);
                    }
                    else
                    {
                        _towerInfoUI.gameObject.SetActive(false);
                        _towerMenuUI.SetActive(true);
                    }
                }
                else
                {
                    _towerInfoUI.gameObject.SetActive(false);
                    _towerMenuUI.SetActive(false);
                    RevertTileSelection();
                }
            }
            else
            {
                _towerInfoUI.gameObject.SetActive(false);
                _towerMenuUI.SetActive(false);
                RevertTileSelection();
            }
        }
        _nonUIPress = false;
    }

    public void OnScreenPress(Vector2 screenPosition, bool isUIPress)
    {
        if (GameManager.Instance.GameOver)
        {
            return;
        }

        if (!isUIPress)
        {
            RevertTileSelection();
            _nonUIPress = true;
            SelectTile(ScreenToTilemapPos(screenPosition));
        }
    }
    #endregion
}
