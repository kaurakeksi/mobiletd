using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using System.Collections.Generic;

[DisallowMultipleComponent]
public class InputHandler : Singleton<InputHandler>
{
    public static event System.Action<Vector2> OnScreenHover = (screenPosition) => { };
    public static event System.Action<Vector2, bool> OnScreenPress = (screenPosition, isUIPress) => { };
    public static event System.Action<Vector2> OnScreenRelease = (screenPosition) => { };

    private Camera _camera;
    private GraphicRaycaster _raycaster;
    private EventSystem _eventSystem;
    private PointerEventData _pointerEventData;
    private Vector2 _currentScreenPosition;
    private List<RaycastResult> _raycastResults;
    private bool _uiPress;

    private void Awake()
    {
        _camera = Camera.main;
        _raycaster = FindObjectOfType<GraphicRaycaster>();
        _eventSystem = FindObjectOfType<EventSystem>();
        _pointerEventData = new PointerEventData(_eventSystem);
        _raycastResults = new List<RaycastResult>();
    }

    private void OnEnable()
    {
        _uiPress = false;
    }

    public void OnHover(InputValue value)
    {
        _currentScreenPosition = value.Get<Vector2>();
        OnScreenHover(_currentScreenPosition);
    }
    public void OnRelease(InputValue value)
    {
        _uiPress = true;
        OnScreenRelease(_currentScreenPosition);
    }

    public void OnPress(InputValue value)
    {
        if (!IsUIAtScreenPosition(_currentScreenPosition))
        {
            _uiPress = false;
        }
        OnScreenPress(_currentScreenPosition, _uiPress);
    }

    // Unity's new input system does not consume inputs that hit UI as of v1.1,
    // so do it manually where needed
    public bool IsUIAtScreenPosition(Vector2 screenPosition)
    {
        _raycastResults.Clear();
        _pointerEventData.position = screenPosition;
        _raycaster.Raycast(_pointerEventData, _raycastResults);
        if (_raycastResults.Count > 0)
        {
            return true;
        }
        return false;
    }
}