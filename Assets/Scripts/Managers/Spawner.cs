﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : Singleton<Spawner>, ITimedUpdate
{
    public static event System.Action<int> OnWaveEnded = (wave) => { };

    [Header("Basic")]
    [SerializeField]
    private GameObject _enemyPrefab;

    [SerializeField]
    private float _waveInterval;

    private Dictionary<int, List<Enemy>> _enemies = new Dictionary<int, List<Enemy>>();
    public Dictionary<int, List<Enemy>> Enemies
    {
        get { return _enemies; }
    }

    private int _currentWave = 0;
    public int CurrentWave
    {
        get { return _currentWave; }
    }

    // Wave spawning
    private bool _spawningWave;
    public bool SpawningWave
    {
        get { return _spawningWave; }
    }
    private float _previousWaveSpawnTime;
    private float _timeUntilNextWave;
    public float TimeUntilNextWave => _timeUntilNextWave;

    // Wave enemy spawning
    private float _previousEnemySpawnTime;
    private float _enemySpawnInterval = 0.4f;
    private Wave _currentWaveData;

    private void OnEnable()
    {
        TimeScaler.Instance.Subscribe(this);
    }

    private void OnDisable()
    {
        TimeScaler.Instance.Unsubscribe(this);
    }

    public void TimedUpdate(float totalTime, float deltaTime)
    {
        if (GameManager.Instance.GameOver)
        {
            return;
        }

        if (_spawningWave)
        {
            if (_currentWaveData.Enemies.Count > 0 && totalTime - _previousEnemySpawnTime > _enemySpawnInterval)
            {
                var enemy = InitializeEnemyFromData(_currentWaveData);
                _currentWaveData.Enemies.RemoveAt(0);
                enemy.gameObject.SetActive(true);

                if (!_enemies.Keys.Contains(_currentWaveData.WaveNumber))
                {
                    _enemies[_currentWaveData.WaveNumber] = new List<Enemy>();
                }
                _enemies[_currentWaveData.WaveNumber].Add(enemy);

                _previousEnemySpawnTime = totalTime;
            }

            if (_currentWaveData.Enemies.Count == 0)
            {
                _spawningWave = false;
                _previousWaveSpawnTime = totalTime;
            }
        }
        else if ((_currentWave > 0 && totalTime - _previousWaveSpawnTime > _waveInterval) ||
                (_currentWave == 0 && totalTime > GameManager.Instance.BuildingTime))
        {
            StartWaveSpawn();
            _spawningWave = true;
        }

        if (_currentWave == 0)
        {
            _timeUntilNextWave = GameManager.Instance.BuildingTime - totalTime;
        }
        else
        {
            _timeUntilNextWave = _waveInterval - (totalTime - _previousWaveSpawnTime);
        }
    }

    private void StartWaveSpawn()
    {
        ++_currentWave;
        _currentWaveData = WaveGenerator.Instance.GetWave(_currentWave);
    }

    public void RemoveEnemy(Enemy enemy)
    {
        int wave = enemy.Wave;
        if (_enemies.Keys.Any(key => key == wave))
        {
            _enemies[wave].Remove(enemy);
            if (_enemies[wave].Count == 0)
            {
                _enemies.Remove(wave);
                OnWaveEnded(wave);
            }
        }
    }

    private Enemy InitializeEnemyFromData(Wave waveData)
    {
        EnemyData enemyData = _currentWaveData.Enemies[0];
        Enemy enemy = ObjectPoolManager.Instance.Instantiate(_enemyPrefab, transform.position, Quaternion.identity).GetComponent<Enemy>();
        if (enemy == null)
        {
            return null;
        }

        enemy.gameObject.SetActive(true);

        enemy.Wave = waveData.WaveNumber;
        enemy.Hitpoints.MaxValue = enemyData.Hitpoints;
        enemy.Hitpoints.BaseValue = enemy.Hitpoints.MaxValue;
        enemy.Type = enemyData.Type;
        enemy.Race = enemyData.Race;
        enemy.transform.localScale *= enemyData.LocalScaleMultiplier;
        enemy.GoldOnKill.BaseValue = enemyData.Gold;
        enemy.ExperienceOnKill.BaseValue = enemyData.Experience;
        enemy.MaxLeakDamage = enemyData.MaxLeakDamage;

        return enemy;
    }
}
