﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaler : Singleton<TimeScaler>
{
    [SerializeField]
    private float _timeStepSize = 1/30.0f;

    [Range(0, 10)]
    [SerializeField]
    private int _timeScale = 1;

    private float _timeOverflow = 0.0f;
    private float _totalTime;    
    private List<ITimedUpdate> _objects = new List<ITimedUpdate>();
    
    public void Subscribe(ITimedUpdate obj)
    {
        if (!_objects.Contains(obj))
        {
            _objects.Add(obj);
        }
    }

    public void Unsubscribe(ITimedUpdate obj)
    {
        _objects.Remove(obj);
    }
    
    private void Update()
    {
        _timeOverflow += Time.deltaTime * _timeScale;
        int wholeTimeSteps = (int) (_timeOverflow / _timeStepSize);

        if (wholeTimeSteps > 0)
        {
            for (int i = 0; i < wholeTimeSteps; ++i)
            {
                for (int j = _objects.Count - 1; j >= 0; --j)
                {
                    j = Mathf.Min(j, _objects.Count - 1);
                    if (_objects[j] != null && _objects[j].gameObject.activeSelf)
                    {
                        _objects[j].TimedUpdate(_totalTime, _timeStepSize);
                    }
                }
                _totalTime += _timeStepSize;
            }
            _timeOverflow -= wholeTimeSteps * _timeStepSize;
            Mathf.Max(_timeOverflow, 0.0f);
        }
    }
}
