﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public static event System.Action OnGameOver = () => { };

    [Header("Start of the game")]
    [Tooltip("The amount of gold player starts the game with.")]
    [SerializeField]
    private int _startingGold;
    public int StartingGold => _startingGold;

    [Tooltip("The amount of towers randomized at the start of the game.")]
    [SerializeField]
    private int _startingTowers;
    public int StartingTowers => _startingTowers;

    [Tooltip("The amount of time before first wave spawns.")]
    [SerializeField]
    private float _buildingTime;
    public float BuildingTime => _buildingTime;

    [Header("Waves")]
    [Tooltip("The amount of damage (in percents) one wave does if it is fully leaked (all enemies at full health).")]
    [SerializeField]
    private float _waveLeakBaseDamage;
    public float WaveLeakBaseDamage => _waveLeakBaseDamage;

    [Tooltip("The amount of health a wave 1 boss would have.")]
    [SerializeField]
    private int _enemyBaseHealth;
    public int EnemyBaseHealth => _enemyBaseHealth;

    [Tooltip("The amount of gold a wave 1 boss would drop.")]
    [SerializeField]
    private int _waveBaseGold;
    public int WaveBaseGold => _waveBaseGold;

    [Tooltip("The amount of experience a boss would give.")]
    [SerializeField]
    private int _waveBaseExp;
    public int WaveBaseExp => _waveBaseExp;

    [Tooltip("The amount of gold given after first wave.")]
    [SerializeField]
    private int _income;
    public int Income => _income;

    private bool _gameOver;
    public bool GameOver => _gameOver;

    private void OnEnable()
    {
        _gameOver = false;
        if (Base.Instance != null)
        {
            Base.OnBaseDead += OnBaseDead;
        }
    }

    private void OnDisable()
    {
        if (Base.Instance != null)
        {
            Base.OnBaseDead -= OnBaseDead;
        }
    }

    public void OnBaseDead()
    {
        _gameOver = true;
        OnGameOver();
    }
}
