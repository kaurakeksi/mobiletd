﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SubWaveData
{
    [SerializeField]
    public EnemyType EnemyType;
    [Range(0, 25)]
    public int MinEnemyCount;
    [Range(0, 25)]
    public int MaxEnemyCount;
}
[CreateAssetMenu(fileName = "Wave", menuName = "Wave/WavePrototype")]
public class WavePrototype : ScriptableObject
{
    public List<SubWaveData> SubWaves;
}
