﻿using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(Stat))]
public sealed class StatDrawer : PropertyDrawer
{
    public const int Gap = 3;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        RefreshValue(property);
        Draw(position, property);
        SanitizeName(property);
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight;
    }

    public static bool IsRequired(SerializedProperty property, Stat stat)
    {
        var obj = property.serializedObject.targetObject as MonoBehaviour;
        if (stat == null)
        {
            return false;
        }

        var components = obj.GetComponents<Component>();
        foreach (var component in components)
        {
            var attributes = component
                .GetType()
                .GetCustomAttributes(typeof(RequireStatAttribute), true);
            if (attributes.Any(x => ((RequireStatAttribute)x).Name == stat.Name))
            {
                return true;
            }
        }
        return false;
    }

    private void SanitizeName(SerializedProperty property)
    {
        var name = property.FindPropertyRelative("_name");
        var nameStr = name.stringValue;
        if (string.IsNullOrEmpty(nameStr))
        {
            nameStr = "Stat";
        }
        name.stringValue = nameStr;
    }

    private void RefreshValue(SerializedProperty property)
    {
        var stat = (Stat)EditorUtil.GetTargetObjectOfProperty(property);
        const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
        var field = stat.GetType().GetField("_isClean", flags);
        field.SetValue(stat, false);
    }

    private void Draw(Rect position, SerializedProperty property)
    {
        var labelWidth = EditorGUIUtility.labelWidth;
        var width = 0.25f * (position.width - labelWidth - 4 * Gap);

        var stat = (Stat)EditorUtil.GetTargetObjectOfProperty(property);
        var isRequired = IsRequired(property, stat);
        var name = property.FindPropertyRelative("_name");
        var baseValue = property.FindPropertyRelative("_baseValue");
        var minValue = property.FindPropertyRelative("_minValue");
        var maxValue = property.FindPropertyRelative("_maxValue");

        if (isRequired)
        {
            EditorGUI.BeginDisabledGroup(true);
        }

        var x = position.x;
        var nameRect = new Rect(x, position.y, labelWidth, position.height);
        name.stringValue = EditorGUI.TextField(nameRect, name.stringValue);

        if (!isRequired)
        {
            EditorGUI.BeginDisabledGroup(true);
        }

        // Show the uneditable current value.
        x += nameRect.width + Gap;
        var currentRect = new Rect(x, position.y, width, position.height);
        EditorGUI.FloatField(currentRect, stat.Value);
        EditorGUI.EndDisabledGroup();

        x += currentRect.width + Gap;
        var baseRect = new Rect(x, position.y, width, position.height);
        var val = EditorGUI.FloatField(baseRect, baseValue.floatValue);
        baseValue.floatValue = Mathf.Clamp(val, minValue.floatValue, maxValue.floatValue);

        x += baseRect.width + Gap;
        var minRect = new Rect(x, position.y, width, position.height);
        minValue.floatValue = EditorGUI.DelayedFloatField(minRect, minValue.floatValue);

        x += minRect.width + Gap;
        var maxRect = new Rect(x, position.y, width, position.height);
        maxValue.floatValue = EditorGUI.DelayedFloatField(maxRect, maxValue.floatValue);
    }

}
