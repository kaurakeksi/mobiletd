using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof(StatCollection))]
public sealed class StatCollectionDrawer : PropertyDrawer
{
    private const int _gapTop = 2;
    private const int _gapBottom = 2;
    private ReorderableList _list;

    private float LabelHeight
    {
        get
        {
            return EditorGUIUtility.singleLineHeight;
        }
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        RenameDuplicates(property);
        Draw(position, property);
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return GetList(property).GetHeight() + LabelHeight;
    }

    private void Draw(Rect position, SerializedProperty property)
    {
        DrawLabel(position);
        position.y += LabelHeight;
        position.height -= LabelHeight;
        GetList(property).DoList(position);
    }

    private void DrawLabel(Rect position)
    {
        var rect = position;
        rect.height = LabelHeight;
        EditorGUI.LabelField(rect, "Stats:");
    }

    private ReorderableList GetList(SerializedProperty property)
    {
        if (_list == null)
        {
            var stats = property.FindPropertyRelative("_stats");
            _list = new ReorderableList(stats.serializedObject, stats, true, true, true, true);
            _list.elementHeight = EditorGUIUtility.singleLineHeight + _gapBottom + _gapTop;

            _list.onCanRemoveCallback = list =>
            {
                var statProperty = list.serializedProperty.GetArrayElementAtIndex(list.index);
                var stat = (Stat)EditorUtil.GetTargetObjectOfProperty(statProperty);
                return !StatDrawer.IsRequired(statProperty, stat);
            };

            _list.drawElementCallback = (rect, index, isActive, isFocused) =>
            {
                rect.y += _gapTop;
                rect.height = rect.height - _gapBottom - _gapTop;
                EditorGUI.PropertyField(rect, stats.GetArrayElementAtIndex(index), true);
            };

            _list.drawHeaderCallback = rect =>
            {
                // Manual adjustment to fix alignment
                rect.x += 12;
                rect.width -= 12;

                var labelWidth = EditorGUIUtility.labelWidth;
                var width = 0.25f * (rect.width - labelWidth - 4 * StatDrawer.Gap);

                var x = rect.x;
                var nameRect = new Rect(x, rect.y, labelWidth, rect.height);
                EditorGUI.LabelField(nameRect, "Name");

                x += nameRect.width + StatDrawer.Gap;
                var currentRect = new Rect(x, rect.y, width, rect.height);
                EditorGUI.LabelField(currentRect, "Value");

                x += currentRect.width + StatDrawer.Gap;
                var baseRect = new Rect(x, rect.y, width, rect.height);
                EditorGUI.LabelField(baseRect, "Base");

                x += baseRect.width + StatDrawer.Gap;
                var minRect = new Rect(x, rect.y, width, rect.height);
                EditorGUI.LabelField(minRect, "Min");

                x += minRect.width + StatDrawer.Gap;
                var maxRect = new Rect(x, rect.y, width, rect.height);
                EditorGUI.LabelField(maxRect, "Max");
            };
        }
        return _list;
    }

    private void RenameDuplicates(SerializedProperty property)
    {
        var names = new HashSet<string>();
        var stats = property.FindPropertyRelative("_stats");
        for (var i = 0; i < stats.arraySize; ++i)
        {
            var stat = stats.GetArrayElementAtIndex(i);
            var name = stat.FindPropertyRelative("_name");
            while(names.Contains(name.stringValue))
            {
                var prefix = Regex.Match(name.stringValue, "^\\D+").Value;
                var numberStr = Regex.Replace(name.stringValue, "^\\D+", "");

                int number;
                if (int.TryParse(numberStr, out number))
                {
                    name.stringValue = prefix + (number + 1);
                }
                else
                {
                    name.stringValue = prefix + 1;
                }
            }
            names.Add(name.stringValue);
        }
    }
}
