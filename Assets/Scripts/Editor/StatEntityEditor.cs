using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(StatEntity), true)]
public sealed class StatEntityEditor : Editor
{
    private StatEntity _entity;

    private void OnEnable()
    {
        _entity = target as StatEntity;
    }

    public override void OnInspectorGUI()
    {
        AddMissingStats();
        DrawDefaultInspector();

        serializedObject.Update();
        DrawStats();
        DrawEvents();
        serializedObject.ApplyModifiedProperties();
    }

    private void DrawStats()
    {
        var stats = serializedObject.FindProperty("_stats");
        EditorGUILayout.PropertyField(stats);
    }

    private void DrawEvents()
    {
    }

    private void AddMissingStats()
    {
        var components = _entity.GetComponents<Component>();
        foreach (var component in components)
        {
            var attributes = component
                .GetType()
                .GetCustomAttributes(typeof(RequireStatAttribute), true);
            foreach (RequireStatAttribute attribute in attributes)
            {
                if (!_entity.Stats.Contains(attribute.Name))
                {
                    var stat = _entity.Stats.Add(attribute.Name);
                    stat.MinValue = attribute.MinValue;
                    stat.MaxValue = attribute.MaxValue;
                    stat.BaseValue = attribute.BaseValue;
                }
            }
        }
    }
}
