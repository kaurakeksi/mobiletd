﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAttackProcSkill : Skill
{
    [Header("Skill specific")]
    [SerializeField]
    private float _chance;

    [SerializeField]
    private float _damage;

    [SerializeField]
    private GameObject _particleEffect;

    public override void TimedUpdate(float totalTime, float deltaTime) { }

    public override void OnAttack()
    {
        if (Random.value <= _chance * _tower.TriggerChances.Value)
        {
            if (_particleEffect != null)
            {
                ObjectPoolManager.Instance.Instantiate(_particleEffect, _tower.Target.transform.position, Quaternion.identity);
            }
            _tower.Target.TakeDamage(_tower);
        }
    }
}
