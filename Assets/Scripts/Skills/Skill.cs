﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Tower))]
public abstract class Skill : MonoBehaviour
{
    [Header("UI")]
    [SerializeField]
    private string _name;
    public string Name => _name;

    [SerializeField]
    private string _description;
    public string Description => _description;

    [SerializeField]
    private Sprite _icon;
    public Sprite Icon => _icon;

    protected Tower _tower;
    protected void Awake()
    {
        _tower = GetComponent<Tower>();
    }

    protected void OnEnable()
    {
        _tower.AddSkill(this);
    }

    protected void OnDisable()
    {
        _tower.RemoveSkill(this);
    }

    public abstract void TimedUpdate(float totalTime, float deltatime);
    public abstract void OnAttack();
}
